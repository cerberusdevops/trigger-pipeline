import time
import json
import yaml
import os

import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_variable, get_logger, enable_debug


BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'

logger = get_logger()

schema = {'BITBUCKET_USERNAME': {'required': True, 'type': 'string'},
          'BITBUCKET_APP_PASSWORD': {'required': True, 'type': 'string'},
          'ACCOUNT': {'nullable': True, 'required': False, 'type': 'string', 'default': os.environ['BITBUCKET_REPO_OWNER']},
          'BRANCH_NAME': {'type': 'string', 'required': False, 'default': 'master'},
          'REF_TYPE': {'allowed': ['branch', 'tag'],
                       'default': 'branch',
                       'type': 'string'},
          'CUSTOM_PIPELINE_NAME': {
                'type': 'string',
                'required': False,
                'nullable': True,
                'default': None
          },
          'PIPELINE_VARIABLES': {
                'required': False,
                'nullable': True,
                'default': None
          },
          'REPOSITORY': {'required': True, 'type': 'string'},
          'WAIT': {'default': False, 'type': 'boolean'},
          'WAIT_MAX_TIMEOUT': {'default': 3600, 'type': 'integer'},
          'DEBUG': {'default': False, 'type': 'boolean'}}


class TriggerBuild(Pipe):

    def _check_build(self, auth, attempt, username, account, repo, build_number, ref_name):

        logger.info(f"Attempt number {attempt}: Getting build info.")

        response = requests.get(
            f'{BITBUCKET_API_BASE_URL}/repositories/{account}/{repo}/pipelines/{build_number}', auth=auth)

        if not response.status_code == 200:
            self.fail(
                f"Failed to get the build data. Response code: {response.status_code}. Error: {response.text}")

        build_data = response.json()
        build_state = build_data['state']['name']

        logger.debug(f"Build state: {build_data['state']}")

        if build_state == 'COMPLETED':
            result = build_data['state']['result']['name']

            if result == 'SUCCESSFUL':
                return True, f"Build number {build_number} for {repo}@{ref_name} finished successfully."
            elif result == 'FAILED':
                return False, f"Build number {build_number} for {repo}@{ref_name} failed."
            else:
                return True, f"Build number {build_number} for {repo}@{ref_name} finished with result {result}."

        elif build_state in ('PENDING', 'IN_PROGRESS'):
            logger.info(
                f"Attempt number {attempt}: Build number {build_number} for {repo} is {build_state}, waiting for pipeline to finish...")
            return None, None

    def _get_tag_info(self, tagname, account, repo, auth):
        logger.info(f'Fetching the tag info for tag:{tagname}')
        response = requests.get(
            f'{BITBUCKET_API_BASE_URL}/repositories/{account}/{repo}/refs/tags/{tagname}', auth=auth)
        if not response.status_code == 200:
            self.fail(
                f'Failed to get the tag info for {tagname} from Bitbucket. Response code: {response.status_code}. Error: {response.text}')
        return response.json()

    def _get_tag_target(self, tagname, username, repo, auth):
        return self._get_tag_info(tagname, username, repo, auth)['target']['hash']

    def run(self):
        super().run()

        token = self.get_variable('BITBUCKET_APP_PASSWORD')
        username = self.get_variable('BITBUCKET_USERNAME')
        account = self.get_variable('ACCOUNT')
        repo = self.get_variable('REPOSITORY')
        ref_name = self.get_variable('BRANCH_NAME')
        custom_pipeline = self.get_variable('CUSTOM_PIPELINE_NAME')
        variables = self.get_variable('PIPELINE_VARIABLES')
        wait = self.get_variable('WAIT')
        wait_max_time = self.get_variable('WAIT_MAX_TIMEOUT')

        headers = {'Content-Type': 'application/json'}
        auth = HTTPBasicAuth(username, token)

        data = {
            "target": {
                "ref_type": 'branch',
                "type": "pipeline_ref_target",
                "ref_name": ref_name
            }
        }

        if custom_pipeline is not None:
            data['target'].update({
                "selector": {
                    "type": 'custom',
                    "pattern": custom_pipeline
                }
            })

        if variables is not None:
            data.update({
                'variables': variables

            })

        response = requests.post(
            f'{BITBUCKET_API_BASE_URL}/repositories/{account}/{repo}/pipelines/', headers=headers, auth=auth, json=data)

        if response.status_code == 404:
            pipe.fail(f"Account, repository or branch doesn't exist.")
        elif response.status_code == 400:
            pipe.fail(f"Error: {response.text}")
        elif response.status_code == 401:
            pipe.fail(
                f"API request failed with status 401. Check your account and app password and try again.")
        elif response.status_code != 201:
            pipe.fail(
                f"Failed to initiate a pipeline. API error code: {response.status_code}. Message: {response.text}")
        else:
            response_data = response.json()
            build_number = response_data['build_number']
            pipe.success(f"Build started successfully. Build number: {build_number}."
                         f"\n\tFollow build logs at the following URL: https://bitbucket.org/{account}/{repo}/addon/pipelines/home#!/results/{build_number}")

        if not wait:
            pipe.success(f"Pipe finished successfully.", do_exit=True)

        logger.info(f"Waiting for pipeline to finish")

        deadline = time.time() + wait_max_time
        attempt = 0
        while time.time() < deadline:
            attempt += 1

            success, message = self._check_build(
                auth, attempt, username, account, repo, build_number, ref_name)

            if success is None:
                time.sleep(5)
            elif not success:
                self.fail(message)
            elif success:
                self.success(message)
                break

        else:
            self.fail('Timeout waiting for the pipeline completion.')

        self.success('Pipe finished successfully.')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = TriggerBuild(pipe_metadata=metadata, schema=schema)
    pipe.run()
